# pytest-flat_message_framework
# Copyright (C) 2020 Szymon Janora

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from .socket_guard import socket_guard_t
from .message_sink import message_sink_t
from .print_info import print_info_send, print_info_receive, \
                        print_info_connect, print_info_receive_any


class message_sink_client_t(object):

    __slots__ = ['name', '__sock', '__sink', 'server']

    def __init__(self, name):
        self.name = name
        self.__sock = socket_guard_t()
        self.__sink = message_sink_t(self.__sock)

    def connect(self, server):
        print_info_connect(self, server)
        self.server = server
        self.__sock.connect(server.get_addr(), server.get_port())

    def send(self, msg):
        print_info_send(self, self.server, msg)
        self.__sink.send_msg(msg)

    def receive(self, expect_msg):
        print_info_receive(self, self.server, expect_msg)
        return self.__sink.receive_msg(expect_msg)

    def receive_any(self, expect_msg_proto):
        return self.receive(expect_msg_proto)

    def try_receive(self, expect_msg_proto, timeout=0.001):
        msg = self.__sink.try_receive_msg(expect_msg_proto, timeout)
        if msg:
            print_info_receive_any(self, self.server, msg)
            return msg
        return None

    def getsockname(self):
        return self.__sock.getsockname()

    def get_addr(self):
        return self.__sock.get_addr()

    def get_port(self):
        return self.__sock.get_port()

    def __hash__(self):
        return hash(self.name)

    def __eq__(self, other):
        return self.name == other.name

    def __str__(self):
        return self.name

    def destroy(self, timeout=0.001):
        if self.__sink:
            self.__sink.destroy(timeout)
            self.__sink = None
        if self.__sock:
            self.__sock.close()
            self.__sock = None

    def __del__(self):
        self.destroy()
