# pytest-flat_message_framework
# Copyright (C) 2020 Szymon Janora

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from example_messages.example_msgs import test_msg_t


def test_msg_flow_should_be_ordered(sink_server, sink_client):
    sink_client.connect(sink_server)
    sink_server.accept(sink_client)
    msg_client_1 = test_msg_t(1000, [0, 1, 2, 3])
    sink_client.send(msg_client_1)
    msg_client_2 = test_msg_t(2000, [0, 1, 2, 3])
    sink_client.send(msg_client_2)
    msg_client_3 = test_msg_t(3000, [0, 1, 2, 3])
    sink_client.send(msg_client_3)
    msg_server_1 = test_msg_t(10000, [0, 1, 2, 3])
    sink_server.send_to(msg_server_1, sink_client)
    msg_server_2 = test_msg_t(10000, [0, 1, 2, 3])
    sink_server.send_to(msg_server_2, sink_client)
    msg_server_3 = test_msg_t(10000, [0, 1, 2, 3])
    sink_server.send_to(msg_server_3, sink_client)
    sink_client.receive(msg_server_1)
    sink_client.receive(msg_server_2)
    sink_server.receive_from(msg_client_1, sink_client)
    sink_server.receive_from(msg_client_2, sink_client)
    sink_client.receive(msg_server_3)
    sink_server.receive_from(msg_client_3, sink_client)
    sink_client.destroy()
    sink_server.destroy()
