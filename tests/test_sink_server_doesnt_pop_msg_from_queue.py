# pytest-flat_message_framework
# Copyright (C) 2020 Szymon Janora

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import pytest
from example_messages.example_msgs import test_msg_t


def test_sink_server_doesnt_pop_msg_from_queue(sink_server, sink_client):
    sink_client.connect(sink_server)
    sink_server.accept(sink_client)
    msg_server = test_msg_t(10000, [0, 1, 2, 3])
    sink_client.send(msg_server)
    sink_client.destroy()
    with pytest.raises(AssertionError, match='The socket queue is not empty!'):
        sink_server.destroy()
