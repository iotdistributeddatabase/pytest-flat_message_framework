# pytest-flat_message_framework
# Copyright (C) 2020 Szymon Janora

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import pytest
# Add the message_framework folder path to the sys.path list
from inspect import getsourcefile
import os.path
import sys
current_path = os.path.abspath(getsourcefile(lambda: 0))
current_dir = os.path.dirname(current_path)
parent_dir = current_dir[:current_dir.rfind(os.path.sep)]
sys.path.insert(0, parent_dir)
# imports for tests
from message_framework.message_sink_client import message_sink_client_t
from message_framework.message_sink_server import message_sink_server_t


def pytest_runtest_setup(item):
    pass


def pytest_runtest_teardown(item):
    pass


@pytest.fixture(scope="function")
def sink_server():
    return message_sink_server_t("sink_server", "::1", 60000, 100)


@pytest.fixture(scope="function")
def sink_client():
    return message_sink_client_t("sink_client")


@pytest.fixture(scope="function")
def sink_client_1():
    return message_sink_client_t("sink_client_1")
