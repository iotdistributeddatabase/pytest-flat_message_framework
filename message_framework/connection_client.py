# pytest-flat_message_framework
# Copyright (C) 2020 Szymon Janora

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

class connection_client_t(object):

    __slots__ = ['name', 'addr', 'port']

    def __init__(self, name, addr='::1', port=0):
        self.name = name
        self.addr = addr
        self.port = port

    def bind_addr(self, addr):
        self.addr = addr[0]
        self.port = addr[1]

    def getsockname(self):
        return (self.addr, self.port)

    def get_addr(self):
        return self.addr

    def get_port(self):
        return self.port

    def __hash__(self):
        return hash(self.name)

    def __eq__(self, other):
        return self.name == other.name

    def __str__(self):
        return self.name
