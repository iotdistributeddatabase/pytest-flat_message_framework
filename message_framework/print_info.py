# pytest-flat_message_framework
# Copyright (C) 2020 Szymon Janora

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import pytest
import subprocess
from abc import ABC
LENGHT_LINE = 90


def plantuml_pytest_addoption(parser):
    parser.addoption(
        "--plantuml",
        action="store",
        dest="plantuml",
        default=None,
        help='Path to dir where plantuml save diagrams')
    parser.addoption(
        "--plantuml-short",
        action="store",
        dest="plantuml-short",
        default=None,
        help='Path to dir where plantuml save diagrams with only msg names')


def plantuml_pytest_configure(config):

    def configure(is_short, path):
        if path:
            plantuml_print_info.set_path(is_short, path)
            config.pluginmanager.register(plantuml_print_info(),
                                          "plantuml_print_info")

    configure(True, config.getoption("--plantuml-short"))
    configure(False, config.getoption("--plantuml"))


class plantuml_print_info:

    output_path = None
    file_plantuml = None
    is_short = False

    def pytest_runtest_setup(self, item):
        type(self).file_plantuml = open(type(self).output_path + '/'
                                        + item.name + '.txt', 'w')
        type(self).file_plantuml.write("@startuml\n")

    def pytest_runtest_teardown(self, item):
        path_file = type(self).file_plantuml.name
        type(self).file_plantuml.write("\n@enduml\n")
        type(self).file_plantuml.flush()
        type(self).file_plantuml.close()
        type(self).file_plantuml = None
        r = subprocess.run(["java", "-jar", "/opt/plantuml/plantuml.jar",
            path_file, "-o", type(self).output_path], capture_output=True)
        if r.returncode:
            raise RuntimeError(r.stderr)

    @classmethod
    def set_path(cls, is_short, output_path):
        cls.is_short = is_short
        cls.output_path = output_path

    @classmethod
    def write_plantuml(cls, format_str, *kargs):
        if cls.file_plantuml:
            cls.file_plantuml.write(format_str.format(*kargs))

    @classmethod
    def write_msg_action(cls, arrow, actorl, actorr, msg):
        msg_name = None
        if type(msg) != type(ABC):
            msg_name = msg.to_name() if cls.is_short else msg.to_str()
        else:
            msg_name = type(msg(*msg.__slots__)).__name__
        cls.write_plantuml(
            '\n"{}" {} "{}" : {}', actorl, arrow, actorr, msg_name)


def write_plantuml_print_info_right(sender, receiver, msg):
    plantuml_print_info.write_plantuml(
        '\n"{}" --> "{}" : {}', sender, receiver, msg)


def write_plantuml_print_info_left(receiver, sender, msg):
    plantuml_print_info.write_plantuml(
        '\n"{}" <-- "{}" : {}', receiver, sender, msg)


def print_info(format_str, *kargs):
    print("\n" + "_"*LENGHT_LINE + "\n" + format_str.format(*kargs))


def print_info_actors(format_str, client, server, *kargs):
    print_info(format_str, client.name, client.getsockname(),
               server.name, server.getsockname(), *kargs)


def print_info_received_msg(msg_received):
    print_info("received_msg {}", msg_received)


def print_info_connect(client, server):
    print_info_actors("connecting {} {} ----> {} {}", client, server)
    write_plantuml_print_info_right(client, server, "Connect")


def print_info_accept(server, client):
    print_info_actors("accepted connection {} {} <---- {} {}", client, server)
    write_plantuml_print_info_left(client, server, "Accept")


def print_info_send(client, server, msg):
    print_info_actors("sending message {} {} ----> {} {}\nsent msg {}",
                      client, server, msg)
    plantuml_print_info.write_msg_action('->', client, server, msg)


def print_info_receive(client, server, msg):
    print_info_actors("waiting for message {} {} <---- {} {}\nreceived msg {}",
                      client, server, msg)
    plantuml_print_info.write_msg_action('<-', client, server, msg)


def print_info_accept_any(server, client):
    print_info_actors("accepted any connection {} {} <---- {} {}",
                      server, client)
    write_plantuml_print_info_left(client, server, "Accept")


def print_info_receive_any(server, client, msg):
    print_info_actors("received any {} {} <---- {} {}\nreceived any msg {}",
          server, client, msg)
    plantuml_print_info.write_msg_action('<-', server, client, msg)
