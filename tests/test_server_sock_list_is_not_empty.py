# pytest-flat_message_framework
# Copyright (C) 2020 Szymon Janora

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import pytest
from example_messages.example_msgs import test_msg_t


def test_server_sock_list_is_not_empty(sink_server,
                                       sink_client, sink_client_1):
    sink_client.connect(sink_server)
    sink_client_1.connect(sink_server)
    sink_server.accept(sink_client)
    with pytest.raises(AssertionError,
                       match=r'The socket list is not empty!(.*)'):
        sink_server.destroy()
    sink_client.destroy()
    sink_client_1.destroy()
