# pytest-flat_message_framework
# Copyright (C) 2020 Szymon Janora

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from abc import ABC, abstractmethod
import flatbuffers
from .message_template import message_template_t


def init_attrs(ob, args, slots):
    for k in slots:
        setattr(ob, k, args[k])


class struct_t(ABC):

    @abstractmethod
    def encode(self, builder):
        pass

    @classmethod
    @abstractmethod
    def decode(cls, stru):
        pass

    def __eq__(self, other):
        """Overrides the default implementation"""
        if isinstance(other, type(self)):
            for key in self.__slots__:
                if getattr(self, key) != getattr(other, key):
                    return False
            return True
        return False

    def __str__(self):
        """Overrides the default implementation"""
        return "<" + type(self).__name__ + ">{" \
               + ";".join('{}="{}"'.format(
                            k, getattr(self, k)) for k in self.__slots__)+"}"


class flatbuffer_message_template_t(message_template_t):

    @classmethod
    def header_size(cls):
        return 4

    @classmethod
    def get_msg_size_from_header(cls, buf, offset=0):
        return flatbuffers.builder.encode.Get(
            flatbuffers.builder.packer.uoffset, buf, offset)

    def pack(self):
        builder = flatbuffers.Builder(0)
        end = self.serialize(builder)
        builder.FinishSizePrefixed(end)
        return builder.Output()


    @abstractmethod
    def serialize(self, builder):
        pass

    @classmethod
    def unpack(cls, buf):
        return cls.deserialize(buf, 0)

    @classmethod
    @abstractmethod
    def deserialize(self, buf, offset):
        pass

    def count(self):
        return len(self.pack())

    def __eq__(self, other):
        """Overrides the default implementation"""
        if isinstance(other, type(self)):
            for key in self.__slots__:
                if getattr(self, key) != getattr(other, key):
                    return False
            return True
        return False

    def __str__(self):
        """Overrides the default implementation"""
        return "<" + type(self).__name__ + ">{" \
               + "; ".join('{}="{}"'.format(
                            k, getattr(self, k)) for k in self.__slots__)+"}"

    def to_name(self):
        return type(self).__name__

    def to_str(self):
        return type(self).__name__ + "{\\n" \
               + "\\n".join('{}="{}"'.format(
                            k, getattr(self, k)) for k in self.__slots__)+"}"
