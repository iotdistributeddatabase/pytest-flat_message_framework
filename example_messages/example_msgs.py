# pytest-flat_message_framework
# Copyright (C) 2020 Szymon Janora

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from abc import abstractmethod
from message_templates.flatbuffer_message_template \
    import flatbuffer_message_template_t, init_attrs, struct_t
from .Example.Msg import *
from .Example.AnyMsgUnion import *
from .Example.TestMsg import *
from .Example.AckRespMsg import *


class msg_template_t(flatbuffer_message_template_t):

    __slots__ = ['type_msg']

    def __init__(self, type_msg):
        self.type_msg = type_msg

    def serialize(self, builder):
        msg = self.encode(builder)
        MsgStart(builder)
        MsgAddMsgType(builder, self.type_msg)
        MsgAddMsg(builder, msg)
        return MsgEnd(builder)

    @abstractmethod
    def encode(self, builder):
        pass

    @classmethod
    def deserialize(cls, buf, offset):
        msg_root = Msg.GetRootAsMsg(buf, offset)
        type = msg_root.MsgType()
        bytes = msg_root.Msg().Bytes
        pos = msg_root.Msg().Pos
        if type in msg_types:
            msg_any, msg_temp = msg_types[type]
        else:
            raise Exception("Unknow msg type id={}!".format(type))
        msg = msg_any()
        msg.Init(bytes, pos)
        return msg_temp.decode(msg)

    @classmethod
    @abstractmethod
    def decode(cls, buf, offset):
        pass


class ack_resp_msg_t(msg_template_t):

    __slots__ = ['status']

    def __init__(self, status):
        super().__init__(AnyMsgUnion.AckRespMsg)
        init_attrs(self, locals(), self.__slots__)

    def encode(self, builder):
        AckRespMsgStart(builder)
        AckRespMsgAddStatus(builder, self.status)
        return AckRespMsgEnd(builder)

    @classmethod
    def decode(cls, msg):
        return ack_resp_msg_t(msg.Status())


class test_msg_t(msg_template_t):

    __slots__ = ['id', 'payload']

    def __init__(self, id, payload):
        super().__init__(AnyMsgUnion.TestMsg)
        init_attrs(self, locals(), self.__slots__)

    def encode(self, builder):
        TestMsgStartPayloadVector(builder, len(self.payload))
        for i in reversed(self.payload):
            builder.PrependByte(i)
        payload = builder.EndVector(len(self.payload))
        TestMsgStart(builder)
        TestMsgAddId(builder, self.id)
        TestMsgAddPayload(builder, payload)
        return TestMsgEnd(builder)

    @classmethod
    def decode(cls, msg):
        payload = []
        for i in range(msg.PayloadLength()):
            payload.append(int(msg.Payload(i)))
        return test_msg_t(msg.Id(), payload)


msg_types = {
        AnyMsgUnion.AckRespMsg: (AckRespMsg, ack_resp_msg_t),
        AnyMsgUnion.TestMsg: (TestMsg, test_msg_t)
        }
