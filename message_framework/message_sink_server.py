# pytest-flat_message_framework
# Copyright (C) 2020 Szymon Janora

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from .socket_guard import socket_guard_t
from .message_sink import message_sink_t
from .connection_client import connection_client_t
import socket
from .print_info import print_info_send, print_info_receive, \
                        print_info_accept, print_info_accept_any, \
                        print_info_receive_any


class message_sink_server_t(object):

    __slots__ = ['name', '__sock', '__connections']

    def __init__(self, name, address, port, list_size):
        self.name = name
        self.__sock = socket_guard_t()
        self.__sock.setsockopt(socket.SOL_SOCKET,
                               socket.SO_REUSEADDR, 1)
        self.__sock.bind(address, port)
        self.__sock.listen(list_size)
        self.__connections = dict()

    def accept(self, client):
        print_info_accept(self, client)
        conn, addr = self.__sock.accept()
        assert conn is not None, 'The socket list is empty!'
        assert addr == client.getsockname(), \
            'It is no client ({})! (addr={}, client={})'.format(
                client, addr, client.getsockname())
        self.__connections[client] = message_sink_t(conn)

    def accept_any(self, client):
        print_info_accept(self, client)
        conn, addr = self.__sock.accept()
        assert conn is not None, 'The socket list is empty!'
        assert addr[0] == client.get_addr(), \
            'It is no client ({})! (addr={}, client={})'.format(
                client, addr, client.getsockname())
        client.bind_addr(addr)
        print_info_accept_any(self, client)
        self.__connections[client] = message_sink_t(conn)

    def try_accept_any(self, timeout=0.001):
        self.__sock.settimeout(timeout)
        conn, addr = None, None
        try:
            conn, addr = self.__sock.accept()
        except socket.timeout:
            pass
        finally:
            self.__sock.settimeout(None)
        if conn:
            client = connection_client_t('client_'+str(addr[1]), addr[0], addr[1])
            self.__connections[client] = message_sink_t(conn)
            print_info_accept_any(self, client)
            return client
        return None

    def send_to(self, msg, client):
        print_info_send(self, client, msg)
        conn = self.__connections[client]
        assert conn is not None, \
            'The connection with {} does not exist!'.format(client)
        conn.send_msg(msg)

    def receive_from(self, expect_msg, client):
        print_info_receive(self, client, expect_msg)
        conn = self.__connections[client]
        assert conn is not None, \
            'The connection with {} does not exist!'.format(client)
        return conn.receive_msg(expect_msg)

    def try_receive_from_any(self, expect_msg_proto, timeout=0.001):
        for client in self.__connections:
            conn = self.__connections[client]
            msg = conn.try_receive_msg(expect_msg_proto, timeout)
            if msg:
                print_info_receive_any(self, client, msg)
                return client, msg
        return None, None

    def getsockname(self):
        return self.__sock.getsockname()

    def get_addr(self):
        return self.__sock.get_addr()

    def get_port(self):
        return self.__sock.get_port()

    def __hash__(self):
        return hash(self.name)

    def __eq__(self, other):
        return self.name == other.name

    def __str__(self):
        return self.name

    def destroy(self, timeout=0.001):
        if self.__sock:
            self.__sock.settimeout(timeout)
            conn, addr = None, None
            try:
                conn, addr = self.__sock.accept()
            except socket.timeout:
                pass
            finally:
                self.__sock.close()
                self.__sock = None
                for key, conn_ref in self.__connections.items():
                    conn_ref.destroy()
                self.__connections.clear()
                self.__connections = None
                assert conn is None, \
                    'The socket list is not empty! (addr={})'.format(addr)

    def __del__(self):
        self.destroy()
