# pytest-flat_message_framework
# Copyright (C) 2020 Szymon Janora

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import pytest
from example_messages.example_msgs import test_msg_t


def test_received_msg_is_not_expected(sink_server, sink_client):
    sink_client.connect(sink_server)
    sink_server.accept(sink_client)
    msg_client = test_msg_t(1000, [0, 1, 2, 3])
    sink_client.send(msg_client)
    msg_client.payload = [101]
    with pytest.raises(AssertionError,
                       match=r'The received message is not expected!(.*)'):
        sink_server.receive_from(msg_client, sink_client)
    sink_client.destroy()
    sink_server.destroy()
