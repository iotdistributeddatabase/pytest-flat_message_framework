# pytest-flat_message_framework
# Copyright (C) 2020 Szymon Janora

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from threading import Barrier, Thread
from subprocess import Popen, PIPE
import os
import signal
import socket
from contextlib import closing
import time
import re


class sut_thread_t(Thread):

    __slots__ = ['__b', '__cmd']

    def __init__(self, b, cmd):
        self.__b = b
        self.__cmd = cmd
        Thread.__init__(self)

    def run(self):
        print("RUN CMD: {}".format(self.__cmd))
        self.__b.wait()
        retval = os.WEXITSTATUS(os.system(self.__cmd))
        if retval != 255:
            print("EXIT CMD {} return value={}".format(self.__cmd, retval))
        else:
            raise Exception("EXIT CMD {} return value={}".format(
                self.__cmd, retval))

    def terminate(self, signal=signal.SIGTERM):
        pipe = Popen('ps -ejf  | grep -E ":[0-9\s]+ [{}]{}"'.format(self.__cmd[0],
                     self.__cmd[1:]), shell=True, stdout=PIPE)
        out, err = pipe.communicate()
        out_strings = out.decode('utf-8').rstrip().split('\n')
        print("\nKilling process: out_strings:\n", "\n".join(
            s for s in out_strings))
        if len(out_strings) >= 1:
            search = re.search(r'\s+(\d+)\s+', out_strings[0])
            if search:
                pid = search.group(0)
                print("\nKilling process: pid=", pid)
                os.kill(int(pid), signal)
        else:
            print("\nKilling process: It is dead!")


class system_under_test_t(object):

    __slots__ = ['__b', 'name', '__address', '__port', '__sut']

    def __init__(self, name, cmd, address, port):
        self.__b = Barrier(2)
        self.name = name
        self.__address = address
        self.__port = port
        self.__sut = sut_thread_t(self.__b, cmd)

    def start(self):
        self.__sut.daemon = True
        self.__sut.start()
        self.__b.wait()
        with closing(socket.socket(socket.AF_INET6, socket.SOCK_STREAM)) as s:
            while self.__sut.is_alive() \
                  and s.connect_ex((self.__address, self.__port)) != 0:
                time.sleep(0.1)

    def stop(self):
        if self.__sut:
            self.__sut.terminate()
            self.__sut.join()
            self.__sut = None

    def destroy(self):
        self.stop()

    def __del__(self):
        self.stop()

    def get_addr(self):
        return self.__address

    def get_port(self):
        return self.__port

    def getsockname(self):
        return (self.__address, self.__port)

    def __hash__(self):
        return hash(self.name)

    def __eq__(self, other):
        return self.name == other.name

    def __str__(self):
        return self.name
