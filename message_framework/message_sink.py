# pytest-flat_message_framework
# Copyright (C) 2020 Szymon Janora

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import socket
import inspect
from .print_info import print_info_received_msg


class message_sink_t(object):

    __slots__ = ['__conn']

    def __init__(self, connection):
        assert connection is not None, 'The connection does not exist!'
        self.__conn = connection

    def send_msg(self, msg):
        self.__conn.sendall(msg.pack())

    def receive_msg(self, expect_msg):
        expect_msg_header_size = expect_msg.header_size()
        msg_header_data = self.__conn.recv(expect_msg_header_size)
        assert len(msg_header_data) == expect_msg_header_size, \
            'The msg header was not received! ({}={})'.format(
                len(msg_header_data), expect_msg_header_size)
        msg_size = expect_msg.get_msg_size_from_header(msg_header_data)
        assert msg_size > 0, \
            'The size msg should not be equal to zero!'
        msg_data = self.__conn.recv(msg_size)
        assert len(msg_data) == msg_size, \
            'The msg received is not equal to expected size ({}={})!'.format(
                len(msg_data), msg_size)
        msg_received = expect_msg.unpack(msg_data)
        print_info_received_msg(msg_received)
        if not inspect.isclass(expect_msg):
            assert expect_msg == msg_received, \
                'The received message is not expected!\
                \n(\n expect_msg={}, \nmsg_receive={}\n)'.format(
                                                     expect_msg, msg_received)
        return msg_received

    def try_receive_msg(self, expect_msg_proto, timeout=0.001):
        expect_msg_header_size = expect_msg_proto.header_size()
        self.__conn.settimeout(timeout)
        msg_header_data = None
        try:
            msg_header_data = self.__conn.recv(expect_msg_header_size)
        except socket.timeout:
            pass
        finally:
            self.__conn.settimeout(None)
        if msg_header_data:
            assert len(msg_header_data) == expect_msg_header_size, \
                'The msg header was not received! ({}={})'.format(
                    len(msg_header_data), expect_msg_header_size)
            msg_size = expect_msg_proto.get_msg_size_from_header(msg_header_data)
            assert msg_size > 0, \
                'The size msg should not be equal to zero!'
            msg_data = self.__conn.recv(msg_size)
            assert len(msg_data) == msg_size, \
                'The msg received is not equal to expected size ({}={})!'.format(
                    len(msg_data), msg_size)
            return expect_msg_proto.unpack(msg_data)
        return None

    def destroy(self, timeout=0.001):
        if self.__conn:
            self.__conn.settimeout(timeout)
            data = b''
            try:
                data = self.__conn.recv(1024)
            except socket.timeout:
                pass
            finally:
                self.__conn.close()
                self.__conn = None
                assert b'' == data, 'The socket queue is not empty!'

    def __del__(self):
        self.destroy()
