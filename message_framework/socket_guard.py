# pytest-flat_message_framework
# Copyright (C) 2020 Szymon Janora

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import socket


class socket_guard_t(object):

    __slots__ = ['__sock', 'address', 'port']

    def __init__(self):
        self.__sock = socket.socket(socket.AF_INET6, socket.SOCK_STREAM, 0)
        assert self.__sock is not None, 'The socket was not created!'

    def __set_sock(self, address, port):
        self.address = address
        self.port = port

    def setsockopt(self, level, optname, value):
        self.__sock.setsockopt(level, optname, value)

    def bind(self, address, port):
        self.__set_sock(address, port)
        self.__sock.bind((self.address, self.port, 0, 0))

    def connect(self, address, port):
        self.__set_sock(address, port)
        self.__sock.connect((self.address, self.port, 0, 0))

    def listen(self, backlog):
        self.__sock.listen(backlog)

    def accept(self):
        return self.__sock.accept()

    def sendall(self, bytes, flags=0):
        return self.__sock.sendall(bytes, flags)

    def recv(self, bufsize, flags=0):
        return self.__sock.recv(bufsize, flags)

    def settimeout(self, value):
        self.__sock.settimeout(value)

    def getsockname(self):
        return self.__sock.getsockname()

    def get_addr(self):
        return self.address

    def get_port(self):
        return self.port

    def close(self):
        self.__sock.close()

    def __del__(self):
        self.close()
