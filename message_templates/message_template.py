# pytest-flat_message_framework
# Copyright (C) 2020 Szymon Janora

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from abc import ABC, abstractmethod


class message_template_t(ABC):

    @abstractmethod
    def header_size(self):
        pass

    @abstractmethod
    def get_msg_size_from_header(self):
        pass

    @abstractmethod
    def pack(self):
        pass

    @classmethod
    def unpack(cls, buf):
        pass

    @abstractmethod
    def count(self):
        pass

    @abstractmethod
    def __eq__(self, other):
        pass

    @abstractmethod
    def __str__(self):
        pass
